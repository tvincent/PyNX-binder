# PyNX dependencies
numpy
cython
scipy
matplotlib
scikit-image
h5py
hdf5plugin
h5glance
silx
pillow
lxml
fabio
ipywidgets
ipympl

# PyNX
http://ftp.esrf.fr/pub/scisoft/PyNX/pynx-latest.tar.bz2

# Jupyter environment
bash_kernel