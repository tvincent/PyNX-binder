# PyNX-binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.esrf.fr%2Ftvincent%2FPyNX-binder.git/a6cd416ac985c9ab1d1701c5c72990c85cce55aa?filepath=index.ipynb)

Binder environment description for [PyNX](http://ftp.esrf.fr/pub/scisoft/PyNX/doc/)